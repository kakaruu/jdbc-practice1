package practice.jdbc1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import practice.jdbc1.service.Constants;

public class Program2 {

	public static void main(String[] args) {
		Connection conn = null;
		PreparedStatement st = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			String title = "TEST";
			String writerId = "god";
			String content = "test content";
			String files = null;
			
			String sql = "INSERT INTO NOTICE "
					+ "(TITLE, WRITER_ID, CONTENT, FILES) "
					+ "VALUES(?, ?, ?, ?)";
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, writerId);
			st.setString(3, content);
			st.setString(4, files);
			
			// 변경된 row의 수를 반환한다.
			int result = st.executeUpdate();
			
			System.out.println(result + "개가 변경되었습니다.");
			
			// ResultSet이 있을 경우 true,
			// Result가 없거나, row count가 반환될 경우 false
			// crud 구분 없이 그냥 사용하고 싶을 때 쓰는 것 같은데
			// 반환값은 어떤 경우에 사용해야 하는지 잘 모르겠다..
//			boolean result = st.execute();
			
			st.close();
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

}
