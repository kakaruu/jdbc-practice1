package practice.jdbc1.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import practice.jdbc1.model.Notice;

// Notice를 전문으로 CRUD, 비즈니스 로직을 처리하는 서비스
public class NoticeService {
	public List<Notice> getList() {
		List<Notice> notices = new ArrayList<Notice>();
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			String sql = "SELECT * FROM NOTICE";
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()) {
				int id = rs.getInt("ID");
				String title = rs.getString("TITLE");
				String writerId = rs.getString("WRITER_ID");
				String content = rs.getString("CONTENT");
				Timestamp regDate = rs.getTimestamp("REGDATE");
				int hit = rs.getInt("HIT");
				String files = rs.getString("FILES");
				
				notices.add(new Notice(id, title, writerId, content, regDate, hit, files));
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(rs != null) {
					rs.close();
					rs = null;
				}
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return notices;
	}

	public int insert(Notice notice) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			String title = notice.getTitle();
			String writerId = notice.getWriterId();
			String content = notice.getContent();
			String files = notice.getFiles();
			
			String sql = "INSERT INTO NOTICE "
					+ "(TITLE, WRITER_ID, CONTENT, FILES) "
					+ "VALUES(?, ?, ?, ?)";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, writerId);
			st.setString(3, content);
			st.setString(4, files);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}

	public int update(Notice notice) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			int id = notice.getId();
			String title = notice.getTitle();
			String content = notice.getContent();
			String files = notice.getFiles();
			
			String sql = "UPDATE NOTICE "
					+ "SET "
					+ "TITLE=?, "
					+ "CONTENT=?, "
					+ "FILES=? "
					+ "WHERE "
					+ "ID=?";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setString(1, title);
			st.setString(2, content);
			st.setString(3, files);
			st.setInt(4, id);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	public int delete(int id) {
		int result = -1;
		
		Connection conn = null;
		PreparedStatement st = null;
		try {
			Class.forName(Constants.DB_DRIVER);
			conn = DriverManager.getConnection(Constants.DB_URL, Constants.DB_USER, Constants.DB_PWD);
			
			String sql = "DELETE NOTICE "
					+ "WHERE "
					+ "ID=?";
			
			// ?로 나중에 값을 넣을 때는 prepareStatement를 사용하면 된다.
			st = conn.prepareStatement(sql);
			st.setInt(1, id);
			
			// 변경된 row의 수를 반환한다.
			result = st.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if(st != null) {
					st.close();
					st = null;
				}
				if(conn != null) {
					conn.close();
					conn = null;
				}
			} catch(SQLException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
}
